UP = "<Up>"
DOWN = "<Down>"
LEFT = "<Left>"
RIGHT = "<Right>"


class Snake():
    def __init__(self):
        self.sections = []
        self.direction = UP

    def add_section(self, section):
        self.sections.append(section)

    def set_direction(self, direction):
        self.direction = direction

    def get_sections(self):
        return self.sections

    def get_direction(self):
        return self.direction

    def reset_snake(self):
        self.__init__()
