# pysnake
The classic Snake game, implemented in the Python 3 programming language

## Dependencies (also in "requirements" file)
tkinter

time

random

## Running
```bash
chmod +x pysnake.py
./pysnake.py
```
OR
```bash
python3 pysnake.py
```
