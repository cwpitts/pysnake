#!/usr/bin/python3

from tkinter import (Tk, Canvas, Menu,
                     Label, StringVar,
                     Entry, Button, TclError)
from snake import Snake, UP, DOWN, LEFT, RIGHT
from time import sleep
import random

HEIGHT = 600
WIDTH = 800
SECTION_SIZE = 10
FOODS = 10
FOOD_SCORE = 10
MOVE = SECTION_SIZE * 2
WELCOME = "Welcome to PySnake!\nPress 'n' to start a new game"

snake = Snake()
food = []
root = None
canvas = None
playing = False
game_cycle = 50
score = 0
scoreText = None
username = None
highscores = []


def make_food():
    for i in range(0, FOODS):
        # Create food ("eten" is Dutch for "food")
        x0 = random.randint(0, WIDTH - SECTION_SIZE)
        y0 = random.randint(0, HEIGHT - SECTION_SIZE)
        x1 = x0 + SECTION_SIZE * 2
        y1 = y0 + SECTION_SIZE * 2
        eten = canvas.create_rectangle(x0, y0, x1, y1, fill="green", width=0)
        # Add to list
        food.append(eten)


def save_score(e, inputWindow):
    global highscores
    global score
    username = e.get()
    highscores.append("{} -- {}\n".format(username, score))
    inputWindow.destroy()


def game_over():
    global playing
    global score
    global username

    playing = False

    try:
        canvas.create_text(WIDTH / 2, HEIGHT / 2,
                           text="Game Over!\nYour score: {}".format(score),
                           justify='center')
    except TclError:
        return
    inputWindow = Tk()
    inputWindow.wm_title = "Enter your name"
    username = StringVar()
    e = Entry(inputWindow, width=30, textvariable=username)
    e.pack()
    e.focus_set()

    b = Button(inputWindow, width=15,
               command=lambda: save_score(e, inputWindow),
               text="Save Score")
    b.pack()
    inputWindow.mainloop()


def update_game():
    global playing
    if playing is False:
        return

    snakeParts = snake.get_sections()
    snakeHead = snakeParts[0]

    x0, y0, x1, y1 = canvas.coords(snakeHead)
    ate = False
    collisions = canvas.find_overlapping(x0, y0, x1, y1)
    for collision in collisions:
        if collision in food:
            # Add score
            global score
            score = score + FOOD_SCORE
            scoreText.set("Score: {}".format(score))

            # Randomly move food
            newx0 = random.randint(0, WIDTH - SECTION_SIZE)
            newy0 = random.randint(0, HEIGHT - SECTION_SIZE)
            newx1 = newx0 + SECTION_SIZE * 2
            newy1 = newy0 + SECTION_SIZE * 2

            canvas.coords(collision, newx0, newy0, newx1, newy1)
            ate = True
            #            move_snake()
        elif collision in snake.get_sections() and collision != snakeHead:
            game_over()
            playing = False

    if ate is True:
        newSection = canvas.create_rectangle(x0, y1,
                                             x1, y1 + SECTION_SIZE * 2,
                                             fill="red", width=0)
        snake.add_section(newSection)

    # print("({}, {}), ({}, {})".format(x0, y0, x1, y1))
    # Check boundaries
    if y0 < 0.0:
        playing = False
    elif y1 > HEIGHT:
        playing = False
    elif x0 < 0.0:
        playing = False
    elif x1 > WIDTH:
        playing = False

    # Move snake
    if playing is False:
        game_over()
        return

    move_snake()

    root.after(game_cycle, update_game)


def move_snake():
    snakeDirection = snake.get_direction()

    sections = snake.get_sections()
    x0 = None
    y0 = None
    x1 = None
    y1 = None
    for k in range(0, len(sections)):
        if k == 0:
            dx = 0
            dy = 0
            if snakeDirection == UP:
                dy = -MOVE
            elif snakeDirection == DOWN:
                dy = MOVE
            elif snakeDirection == LEFT:
                dx = -MOVE
            elif snakeDirection == RIGHT:
                dx = MOVE

            x0, y0, x1, y1 = canvas.coords(sections[0])
            canvas.move(sections[0], dx, dy)
        else:
            oldx0, oldy0, oldx1, oldy1 = canvas.coords(sections[k])
            dx = x0 - oldx0
            dy = y0 - oldy0
            x0, y0, x1, y1 = canvas.coords(sections[k])
            canvas.move(sections[k], dx, dy)


def handle_key(keyEvent):
    if keyEvent.char == 'q':
        root.destroy()
        save_highscores()
    elif keyEvent.char == 'n':
        new_game()
    elif keyEvent.char == 'p':
        global playing
        playing = not playing
        root.after(game_cycle, update_game)


def handle_arrow(arrow):
    if playing is False:
        return

    snake.set_direction(arrow)


def countdown():
    for i in range(0, 3):
        t = canvas.create_text(WIDTH / 2, HEIGHT / 2,
                               text=str(i + 1), justify='center')
        sleep(1)
        canvas.delete(t)


def save_highscores():
    global highscores
    with open("scores", 'w') as f:
        for score in highscores:
            f.write(score)


def new_game():
    global score
    canvas.delete("all")
    center = (WIDTH / 2, HEIGHT / 2)

    x0 = center[0] - SECTION_SIZE
    y0 = center[1] - SECTION_SIZE
    x1 = center[0] + SECTION_SIZE
    y1 = center[1] + SECTION_SIZE

    make_food()

    snake.reset_snake()
    score = 0
    scoreText.set("Score: {}".format(score))

    section = canvas.create_rectangle(x0, y0, x1, y1, fill="red", width=0)
    snake.add_section(section)

    # Count down to start
    global playing
    playing = True
    root.after(game_cycle, update_game)


def make_menu(root):
    menubar = Menu(root)

    filemenu = Menu(menubar, tearoff=0)
    filemenu.add_command(label="New Game", command=new_game)
    filemenu.add_command(label="Scores", command=show_scores)

    menubar.add_cascade(label="File", menu=filemenu)
    return menubar


def show_scores():
    global highscores
    scoreWindow = Tk()
    scoreWindow.wm_title = "Scores"

    height = 400
    width = 200

    c = Canvas(scoreWindow, height=height, width=width)
    c.pack()

    highscores.sort(key=lambda x: int(x.split(" -- ")[1]), reverse=True)

    body = "The PySnake Scoreboard\n"
    for score in highscores:
        body = body + score

    c.create_text(width / 2, height / 2,
                  text=body,
                  justify="center")

    try:
        scoreWindow.mainloop()
    except KeyboardInterrupt:
        pass


def load_highscores():
    global highscores
    with open("scores", 'r') as f:
        for line in f:
            highscores.append(line)


if __name__ == "__main__":
    # Create root window and canvas
    root = Tk()
    root.wm_title("PySnake")

    canvas = Canvas(root, height=HEIGHT, width=WIDTH)
    canvas.pack()
    canvas.create_text(WIDTH / 2, HEIGHT / 2,
                       text=WELCOME,
                       justify="center")

    scoreText = StringVar()
    scoreText.set("Score: {}".format(score))
    scorebox = Label(root, textvariable=scoreText)
    scorebox.pack()

    root.bind("<Key>", handle_key)
    # Binding for arrow keys

    root.bind("<Right>", lambda x: handle_arrow("<Right>"))
    root.bind("<Left>", lambda x: handle_arrow("<Left>"))
    root.bind("<Down>", lambda x: handle_arrow("<Down>"))
    root.bind("<Up>", lambda x: handle_arrow("<Up>"))

    root.config(menu=make_menu(root))

    load_highscores()

    try:
        root.mainloop()
    except KeyboardInterrupt:
        pass
    finally:
        save_highscores()
